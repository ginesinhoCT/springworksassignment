//
//  String+JsonTests.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import XCTest

class String_JsonTests: XCTestCase {
    
    let jsonSample = "{\"regno\": \"wur816\", \"vin\": \"tmbga61z852094863\", \"timestamp\": \"2015-05-29T12:18:31.390Z\", \"emission\":{\"gasoline\": {\"co2\": {\"mixed\": 0.000175}}}}"
    
    let notJsonSample = "{\"regno\": \"wur816\", \"vin\": \"tmbga61z852094863\", There is an error here}"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testSerializationSuccessIsNotNil() {
        XCTAssertNotNil(jsonSample.serializeJsonToDictionary(), "Error: Serialization is equal to nil.")
    }
    
    func testSerializationFailsIsNil() {
        XCTAssertNil(notJsonSample.serializeJsonToDictionary(), "Error: Serialization is not equal to nil.")
    }
    
    func testSerializationSuccessRightData() {
        guard let dict = jsonSample.serializeJsonToDictionary() else { return }
        XCTAssertTrue(dict["regno"] as! String == "wur816", "Error: Wrong json serialization.")
        XCTAssertTrue(dict["vin"] as! String == "tmbga61z852094863", "Error: Wrong json serialization.")
        XCTAssertTrue(dict["timestamp"] as! String == "2015-05-29T12:18:31.390Z", "Error: Wrong json serialization.")
        
        //TODO: find the way to get the dictionary directly
        let emission = dict["emission"] as! [String: Any]
        let gasoline = emission["gasoline"] as! [String: Any]
        let co2 = gasoline["co2"] as! [String: Any]
        XCTAssertTrue(co2["mixed"] as! NSNumber == 0.000175, "Error: Wrong json serialization.")
    }
}
