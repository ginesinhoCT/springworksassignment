//
//  Number+MetricSystemTests.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import XCTest

class Number_MetricSystemTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExtensionMethods() {
        
        let literPerMeter = NSNumber(value: 0.000099)
        XCTAssertTrue(literPerMeter.litresPerMetreTolitresPer100km().stringValueWithOneDecimals() == "9.9",
                      "Error: Wrong litres per 100km string value.")
        XCTAssertTrue(literPerMeter.litresPerMetreToMilesPerGallon().stringValueWithTwoDecimals() == "23.76",
                      "Error: Wrong miles per gallon string value.")

        let kgPerMeter = NSNumber(value: 0.000175)        
        XCTAssertTrue(kgPerMeter.kgPerMeterToGramsPerMile().stringValueWithEightDecimals() == "0.00028163",
                      "Error: Wrong grams per mile string value.")
    }
}
