//
//  String+DateTests.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import XCTest

class String_DateTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testExtensionMethods() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: "2015-05-29T12:18:31.390Z")!
        
        XCTAssertTrue("2015-05-29T12:18:31.390Z".dateWithFormatISO8601() == date,
                      "Error: Dates are not equal.")

    }
    
}
