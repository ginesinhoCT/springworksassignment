//
//  Float+FormattedStringTests.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import XCTest

class Float_FormattedStringTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testExtensionMethods() {
        
        let float = 1.23456789
        XCTAssertTrue(float.stringValueWithOneDecimals() == "1.2", "Error: Wrong one decimal string value.")
        XCTAssertTrue(float.stringValueWithTwoDecimals() == "1.23", "Error: Wrong two decimals string value.")
        XCTAssertTrue(float.stringValueWithEightDecimals() == "1.23456789", "Error: Wrong eight decimals string value.")
    }
}
