//
//  VehicleInfoTests.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 14/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import XCTest

class VehicleInfoTests: XCTestCase {
    
    let sampleJson = "{\"regno\": \"wur816\", \"vin\": \"tmbga61z852094863\", \"timestamp\": \"2015-05-29T12:18:31.390Z\", \"emission\":{\"gasoline\": {\"co2\": {\"mixed\": 0.000175}}},\"fuel\": {\"gasoline\": {\"average_consumption\": {\"urban\": 0.000099,\"rural\": 0.000058,\"mixed\": 0.000073}}},\"gearbox_type\": \"manual\",\"year\": 2005,\"brand\": \"volvo\",\"fuel_types\": [\"gasoline\"]}"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    
    func testVehicleUSAInfoComputedProperties() {
        
        guard let dict = sampleJson.serializeJsonToDictionary() else { return }
        let vehicleInfo = VehicleUSAInfoStruct(dictionary: dict)
        XCTAssertTrue(vehicleInfo.regnoString == "WUR816", "Error: Wrong regno string.")
        XCTAssertTrue(vehicleInfo.vinString == "TMBGA61Z852094863", "Error: Wrong vin string.")
        XCTAssertTrue(vehicleInfo.gearTypeString == "Manual", "Error: Wrong gear type string.")
        XCTAssertTrue(vehicleInfo.yearString == "2005", "Error: Wrong year string.")
        XCTAssertTrue(vehicleInfo.fuelTypesArray == ["gasoline"], "Error: Wrong fuel types array.")
        XCTAssertEqual(vehicleInfo.brandLogoImage, UIImage(named:"volvoLogo"), "Error: Wrong brand logo image.")
        
        print(vehicleInfo.fuelInfosArray)
        let fuelInfoDetails = vehicleInfo.fuelInfosArray[0]
        let fuelInfo = fuelInfoDetails["gasoline"] as! Dictionary<String, Any>
        let consupmtion = fuelInfo["average_consumption"] as! Dictionary<String, String>
        XCTAssertTrue(consupmtion["urban"] == "23.8", "Error: Wrong urban average consuption string value.")
        XCTAssertTrue(consupmtion["rural"] == "40.6", "Error: Wrong rural average consuption string value.")
        XCTAssertTrue(consupmtion["mixed"] == "32.2", "Error: Wrong mixed average consuption string value.")

        let emissions = fuelInfo["co2"] as! Dictionary<String, String>
        XCTAssertTrue(emissions["mixed"] == "0.00028163", "Error: Wrong mixed co2 emissions string value.")
        
        XCTAssertTrue(vehicleInfo.timestampDate == "2015-05-29T12:18:31.390Z".dateWithFormatISO8601(), "Error: Wrong json serialization.")
    }
    
}
