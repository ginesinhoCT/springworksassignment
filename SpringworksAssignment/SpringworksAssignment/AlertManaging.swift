//
//  AlertManaging.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 16/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation
import UIKit

protocol AlertManaging {
    
    /**
     Show error alert view with error title, message and controller that presents it.
     
     - parameters:
        - error: String with alert title
        - message: String with alert message
        - delegate: UIViewController that presents alert view
     */
    func showErrorAlert(error: String, message: String, delegate: UIViewController)
}

extension AlertManaging {
    
    //Default implementation using UIAlertController
    
    func showErrorAlert(error: String, message: String, delegate: UIViewController) {
        let alert = UIAlertController(title: error, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertActionStyle.default, handler: nil))
        delegate.present(alert, animated: true, completion: nil)
    }
}
