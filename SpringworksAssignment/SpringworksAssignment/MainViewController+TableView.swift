//
//  MainViewController+TableView.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 16/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation
import UIKit

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let array = self.fuelInfosArray {
            return array.count
        }
        
        //There is always a cell even when the information is not available
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FuelInfoCell") as! FuelInfoCell
        
        //Title Label set up
        cell.urbanTitleLabel.text = "urban".localized
        cell.ruralTitleLabel.text = "rural".localized
        cell.mixedTitleLabel.text = "mixed".localized
        cell.consumptionTitleLabel.text = "Consumptions".localized
        cell.emissionsTitleLabel.text = "Emissions".localized
        
        if let array = self.fuelInfosArray {
            
            let fuelInfo = array[indexPath.row] as Dictionary<String, Any>
            let fuelInfoName = Array(fuelInfo.keys)[0]
            
            cell.fuelTypeLabel.text = fuelInfoName.localized
            
            //Data Label set up            
            let fuelDetailsInfo = fuelInfo[fuelInfoName] as! Dictionary<String, Any>
            let consupmtionsDetailsInfo = fuelDetailsInfo["average_consumption"] as! Dictionary<String, String>
            
            if let urbanConsupmtion = consupmtionsDetailsInfo["urban"] {
                cell.urbanConsupmtionLabel.text = urbanConsupmtion
            }else {
                cell.urbanConsupmtionLabel.text = "n/a".localized
            }
            
            if let ruralConsupmtion = consupmtionsDetailsInfo["rural"] {
                cell.ruralConsumptionLabel.text = ruralConsupmtion
            }else {
                cell.ruralConsumptionLabel.text = "n/a".localized
            }
            
            if let mixedConsupmtion = consupmtionsDetailsInfo["mixed"] {
                cell.mixedConsupmtionLabel.text = mixedConsupmtion
            }else {
                cell.mixedConsupmtionLabel.text = "n/a".localized
            }
            
            let emissionsDetailsInfo = fuelDetailsInfo["co2"] as! Dictionary<String, String>
            
            if let urbanEmissions = emissionsDetailsInfo["urban"] {
                cell.urbanEmissionsLabel.text = urbanEmissions
            }else {
                cell.urbanEmissionsLabel.text = "n/a".localized
            }
            
            if let ruralEmissions = emissionsDetailsInfo["rural"] {
                cell.ruralEmissionsLabel.text = ruralEmissions
            }else {
                cell.ruralEmissionsLabel.text = "n/a".localized
            }
            
            if let mixedEmissions = emissionsDetailsInfo["mixed"] {
                cell.mixedEmissionsLabel.text = mixedEmissions
            }else {
                cell.mixedEmissionsLabel.text = "n/a".localized
            }
        } else {
            
            cell.fuelTypeLabel.text = "Fuel".localizedWith(info: "n/a".localized)
            
            //Data Label set up
            cell.urbanConsupmtionLabel.text = "n/a".localized
            cell.ruralConsumptionLabel.text = "n/a".localized
            cell.mixedConsupmtionLabel.text = "n/a".localized
            cell.urbanEmissionsLabel.text = "n/a".localized
            cell.ruralEmissionsLabel.text = "n/a".localized
            cell.mixedEmissionsLabel.text = "n/a".localized
            
        }
        
        return cell
    }

}
