//
//  MainViewController+UserDefaults.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 16/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation

extension MainViewController {
    
    //We have here the implementation because the Date extension is not recognized by the compiler
    
    func setInUserDefaults(date: Date, forkey key: String) {
        
        let userDefults = UserDefaults.standard
        userDefults.set(date, forKey: key)
    }
    
    func getDateFromUserDefaults(forkey key: String) -> Date? {
        
        let userDefults = UserDefaults.standard
        
        if let storedDate = userDefults.value(forKey: key) as? Date{
            return storedDate
        } else {
            return nil
        }
    }
}
