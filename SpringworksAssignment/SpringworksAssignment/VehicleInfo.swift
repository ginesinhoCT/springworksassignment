//
//  CarInfo.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 14/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation
import UIKit

protocol VehicleInfo {
    
    // MARK: - Model definition
    
    /// String with the registration number.
    var regno: String { get }
    
    /// String with the vin (Vehicle Identification Number).
    var vin: String { get }
    
    /// String with the brand of the vehicle.
    var brand: String { get }
    
    /// Number with the vehicle model year.
    var year: NSNumber { get }
    
    /// String with the type of gearbox.
    var gearboxType: String { get }
    
    /// Array with the possible fuel types. At least one is provided.
    var fuelTypes: Array<String> { get }
    
    /// Dictionary with the fuel information: average consumption, average consumption mixed...
    var fuel: Dictionary<String, Any> { get }
    
    /// Dictionary with the emission information: average emissions, average emissions mixed...
    var emission: Dictionary<String, Any> { get }
    
    /// String wit the time of the last update (iso-8601 format in UTC)
    // The API documentation is wrong. We don't get a Date
    var timestamp: String { get }
}

struct VehicleUSAInfoStruct: VehicleInfo {
    
    // MARK: - Model implementation
    
    //Corforming VehicleInfo protocol
    internal var regno: String
    internal var vin: String
    internal var brand: String
    internal var year: NSNumber
    internal var gearboxType: String
    internal var fuelTypes: Array<String>
    internal var fuel: Dictionary<String, Any>
    internal var emission: Dictionary<String, Any>
    internal var timestamp: String
    
    //Custom initialiazer
    init(dictionary: Dictionary<String, Any>) {
        
        regno       = dictionary["regno"] as! String
        vin         = dictionary["vin"] as! String
        brand       = dictionary["brand"] as! String
        year        = dictionary["year"] as! NSNumber
        gearboxType = dictionary["gearbox_type"] as! String
        fuelTypes   = dictionary["fuel_types"] as! Array
        fuel        = dictionary["fuel"] as! Dictionary
        emission    = dictionary["emission"] as! Dictionary
        timestamp   = dictionary["timestamp"] as! String
    }
    
    // MARK: - Computed properties (In MVVM would be ViewModel)
    
    /// Formatted and localizated String with registration number.
    var regnoString: String {
        return regno.uppercased()
    }
    
    /// Formatted and localizated String with vin (Vehicle Identification Number).
    var vinString: String {
        return vin.uppercased()
    }
    
    /// Formatted and localizated String with type of gearbox.
    var gearTypeString: String {
        return gearboxType.capitalized
    }
    
    /// Formatted and localizated String with vehicle model year.
    var yearString: String {
        return String(describing: year.decimalValue)
    }
    
    /// UIImage with brand logo.
    var brandLogoImage: UIImage {
        let logoName = "\(brand)Logo"
        return UIImage(named:logoName)!
    }
    
    /// Array with the computed fuel types. At least one is provided.
    var fuelTypesArray: Array<String> {
        return fuelTypes 
    }
    
    /// Dictionary with the average consuption of each fuel in miles per gallon
    var fuelInfosArray: [Dictionary<String, Any>] {
        
        var array: [Dictionary<String, Any>] = []
        
        for fuelType in fuelTypes {
            
            var fuelDict: Dictionary<String, Any> = Dictionary()
            var fuelDetailsDict: Dictionary<String, Any> = Dictionary()
            var consumptionsDict: Dictionary<String, String> = Dictionary()
            var emissionsDict: Dictionary<String, String> = Dictionary()
            
            //the dictionary hierarchy is: fuel -> [fuel_type] -> average_consumption -> [urban, rural, mixed]
            let averageConsumptionDictionary = fuel[fuelType] as! Dictionary<String, Any>
            var consupmtionDetailsDictionary = averageConsumptionDictionary["average_consumption"] as! Dictionary<String, NSNumber>
                        
            if let urban = consupmtionDetailsDictionary["urban"] {
                consumptionsDict["urban"] = urban.litresPerMetreToMilesPerGallon().stringValueWithOneDecimals()
            }
            
            if let rural = consupmtionDetailsDictionary["rural"] {
                consumptionsDict["rural"] = rural.litresPerMetreToMilesPerGallon().stringValueWithOneDecimals()
            }
            
            if let mixed = consupmtionDetailsDictionary["mixed"] {
                consumptionsDict["mixed"] = mixed.litresPerMetreToMilesPerGallon().stringValueWithOneDecimals()
            }
            
            fuelDetailsDict["average_consumption"] = consumptionsDict
            
            //the dictionary hierarchy is: emission -> [fuel_type] -> co2 -> [urban, rural, mixed]
            let averageEmissionsDictionary = emission[fuelType] as! Dictionary<String, Any>
            let emissionDetailsDictionary = averageEmissionsDictionary["co2"] as! Dictionary<String, NSNumber>
            
            if let urban = emissionDetailsDictionary["urban"] {
                emissionsDict["urban"] = urban.kgPerMeterToGramsPerMile().stringValueWithEightDecimals()
            }
            
            if let rural = emissionDetailsDictionary["rural"] {
                emissionsDict["rural"] = rural.kgPerMeterToGramsPerMile().stringValueWithEightDecimals()
            }
            
            if let mixed = emissionDetailsDictionary["mixed"] {
                emissionsDict["mixed"] = mixed.kgPerMeterToGramsPerMile().stringValueWithEightDecimals()
            }
            
            fuelDetailsDict["co2"] = emissionsDict
            fuelDict[fuelType] = fuelDetailsDict
            array.append(fuelDict)
        }
        
        return array
        
    }
    
    /// Date wit the time of the last update in iso-8601 format in UTC.    
    var timestampDate: Date {
        return timestamp.dateWithFormatISO8601()
    }
}
