//
//  Number+USAMetricSystem.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation

extension NSNumber {
    
    /**
     Convert litres per meter to litres per 100 km from a double
     
     - returns:
     A *Double* with the conversion to litres per 100 km
     */
    func litresPerMetreTolitresPer100km() -> Double {
        //100 km = 100000 meters
        return self.doubleValue * 100000
    }
    
    /**
     Convert litres per meter to miles per gallon from a double
     
     - returns:
     A *Double* with the conversion to miles per gallon
     */
    func litresPerMetreToMilesPerGallon() -> Double {
        //1 US mpg = 235.215 l/100 km
        return 235.215 / self.litresPerMetreTolitresPer100km()
    }
    
    /**
     Convert kilograms per meter to grams per mile from a double
     
     - returns:
     A *Double* with the conversion to grams per mile
     */
    func kgPerMeterToGramsPerMile() -> Double {
        //1 mile = 1.60934 km
        //1 kg/m = 1 gr/km        
        return self.doubleValue * 1.60934
    }
}
