//
//  APIManaging.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 14/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation
import Alamofire

protocol APIManaging {
    
    // FIXME: there is a bug in swift and we can't use @escaping with optional blocks:
    //https://bugs.swift.org/browse/SR-2444
    //That's why we are not using the next associated type:
    //associatedtype ServiceResponse = (Dictionary<String, Any>?) -> Void
    
    /**
     Make a GET request.
     
     - parameters:
     - url: String of the url
     - onCompletion: Closure with a Dictionary (json) and a String (error description) as parameters
     
     - Important:
     There are two possibilities of callback:
        - (json dictionary, nil): we get the json from the request
        - (nil, error localized description): we got an error from the request
     */
    func getRequest(_ url: String, onCompletion: @escaping (Dictionary<String, Any>?, String?) -> Void)
}

extension APIManaging {
    
    //Default implementation using AlamoFire
    
    func getRequest(_ url: String, onCompletion: @escaping (Dictionary<String, Any>?, String?) -> Void) {
        
        Alamofire.request(url).responseJSON { response in
            
            if let error = response.error {
                onCompletion(nil, error.localizedDescription)
            }else {
                onCompletion(response.result.value as? Dictionary<String, Any>, nil)
            }
        }
    }
}
