//
//  Number+FormattedString.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation

extension Double {
    
    /**
      Create a string with the double value formatted with one decimal
     
     - returns:
     A *String* from the double value with one decimal
     */
    func stringValueWithOneDecimals() -> String {
        return String(format: "%.1f", self)
    }
    
    /**
     Create a string with the double value formatted with four decimals
     
     - returns:
     A *String* from the double value with four decimals
     */
    func stringValueWithTwoDecimals() -> String {
        return String(format: "%.2f", self)
    }
    
    /**
     Create a string with the double value formatted with eight decimals
     
     - returns:
     A *String* from the double value with eight decimals
     */
    func stringValueWithEightDecimals() -> String {
        return String(format: "%.8f", self)
    }
}
