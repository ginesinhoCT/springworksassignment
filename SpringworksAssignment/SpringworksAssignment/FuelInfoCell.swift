//
//  FuelInfoCellTableViewCell.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import UIKit

@IBDesignable

class FuelInfoCell: UITableViewCell {
    
    @IBOutlet weak var fuelTypeLabel: UILabel!
    @IBOutlet weak var consumptionTitleLabel: UILabel!
    @IBOutlet weak var emissionsTitleLabel: UILabel!
    @IBOutlet weak var urbanTitleLabel: UILabel!
    @IBOutlet weak var ruralTitleLabel: UILabel!
    @IBOutlet weak var mixedTitleLabel: UILabel!
    @IBOutlet weak var urbanConsupmtionLabel: UILabel!
    @IBOutlet weak var ruralConsumptionLabel: UILabel!
    @IBOutlet weak var mixedConsupmtionLabel: UILabel!
    @IBOutlet weak var urbanEmissionsLabel: UILabel!    
    @IBOutlet weak var ruralEmissionsLabel: UILabel!
    @IBOutlet weak var mixedEmissionsLabel: UILabel!
    
    override func draw(_ rect: CGRect) {        
        //Ad border line and modify view corner radius
        self.layer.borderWidth = 2
        self.layer.borderColor = SWAColors.lightBlue.cgColor
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 5.0
        
    }
}
