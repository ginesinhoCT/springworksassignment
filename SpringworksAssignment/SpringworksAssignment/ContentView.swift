//
//  LogoImageView.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import UIKit

@IBDesignable

class ContentView: UIView {
    
    override func draw(_ rect: CGRect) {
        
        //Ad border line and modify view corner radius
        self.layer.borderWidth = 2
        self.layer.borderColor = SWAColors.lightBlue.cgColor
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 5.0
        
    }
}
