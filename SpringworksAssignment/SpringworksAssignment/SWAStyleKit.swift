//
//  SWAStyleKit.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez on 15/2/17.
//  Copyright © 2017 Gines Sanchez. All rights reserved.
//
//  Generated by PaintCode
//  http://www.paintcodeapp.com
//



import UIKit

public class SWAStyleKit : NSObject {

    //// Drawing Methods

    public dynamic class func drawReloadBarButton(frame: CGRect = CGRect(x: 0, y: 0, width: 40, height: 40), strokeColor: UIColor = UIColor(red: 0.573, green: 0.061, blue: 0.097, alpha: 1.000), strokeArrow: CGFloat = 3.5) {
        //// General Declarations
        // This non-generic function dramatically improves compilation times of complex expressions.
        func fastFloor(_ x: CGFloat) -> CGFloat { return floor(x) }


        //// Subframes
        let circleArrow: CGRect = CGRect(x: frame.minX + fastFloor(frame.width * 0.14026 - 0.11) + 0.61, y: frame.minY + fastFloor(frame.height * 0.09314 - 0.23) + 0.73, width: fastFloor(frame.width * 0.85974 + 0.11) - fastFloor(frame.width * 0.14026 - 0.11) - 0.22, height: fastFloor(frame.height * 0.89718 - 0.39) - fastFloor(frame.height * 0.09314 - 0.23) + 0.16)


        //// CircleArrow
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: circleArrow.minX + 0.72581 * circleArrow.width, y: circleArrow.minY + 0.15395 * circleArrow.height))
        bezierPath.addCurve(to: CGPoint(x: circleArrow.minX + 0.46137 * circleArrow.width, y: circleArrow.minY + 0.10518 * circleArrow.height), controlPoint1: CGPoint(x: circleArrow.minX + 0.66099 * circleArrow.width, y: circleArrow.minY + 0.11453 * circleArrow.height), controlPoint2: CGPoint(x: circleArrow.minX + 0.51016 * circleArrow.width, y: circleArrow.minY + 0.10052 * circleArrow.height))
        bezierPath.addCurve(to: CGPoint(x: circleArrow.minX + 0.00006 * circleArrow.width, y: circleArrow.minY + 0.55257 * circleArrow.height), controlPoint1: CGPoint(x: circleArrow.minX + 0.14702 * circleArrow.width, y: circleArrow.minY + 0.13520 * circleArrow.height), controlPoint2: CGPoint(x: circleArrow.minX + -0.00342 * circleArrow.width, y: circleArrow.minY + 0.36280 * circleArrow.height))
        bezierPath.addCurve(to: CGPoint(x: circleArrow.minX + 0.49987 * circleArrow.width, y: circleArrow.minY + 0.99982 * circleArrow.height), controlPoint1: CGPoint(x: circleArrow.minX + 0.00405 * circleArrow.width, y: circleArrow.minY + 0.76751 * circleArrow.height), controlPoint2: CGPoint(x: circleArrow.minX + 0.18466 * circleArrow.width, y: circleArrow.minY + 0.99142 * circleArrow.height))
        bezierPath.addCurve(to: CGPoint(x: circleArrow.minX + 0.99968 * circleArrow.width, y: circleArrow.minY + 0.55257 * circleArrow.height), controlPoint1: CGPoint(x: circleArrow.minX + 0.78617 * circleArrow.width, y: circleArrow.minY + 1.00743 * circleArrow.height), controlPoint2: CGPoint(x: circleArrow.minX + 1.00962 * circleArrow.width, y: circleArrow.minY + 0.77059 * circleArrow.height))
        strokeColor.setStroke()
        bezierPath.lineWidth = strokeArrow
        bezierPath.miterLimit = 1.5
        bezierPath.lineCapStyle = .round
        bezierPath.lineJoinStyle = .round
        bezierPath.stroke()


        //// Bezier 2 Drawing
        let bezier2Path = UIBezierPath()
        bezier2Path.move(to: CGPoint(x: circleArrow.minX + 0.59154 * circleArrow.width, y: circleArrow.minY + 0.27734 * circleArrow.height))
        bezier2Path.addLine(to: CGPoint(x: circleArrow.minX + 0.98066 * circleArrow.width, y: circleArrow.minY + 0.36384 * circleArrow.height))
        bezier2Path.addLine(to: CGPoint(x: circleArrow.minX + 0.84534 * circleArrow.width, y: circleArrow.minY + 0.00000 * circleArrow.height))
        strokeColor.setStroke()
        bezier2Path.lineWidth = strokeArrow
        bezier2Path.miterLimit = 1.5
        bezier2Path.lineCapStyle = .round
        bezier2Path.lineJoinStyle = .round
        bezier2Path.stroke()
    }

}
