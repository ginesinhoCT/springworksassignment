//
//  SWAUrls.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 16/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation

struct SWAUrls {
    
    static let vehicleInfo = "https://gist.githubusercontent.com/sommestad/e38c1acf2aed495edf2d/raw/cdb6dfb85101eedad60853c44266249a3f4ac5df/vehicle-attributes.json"
}
