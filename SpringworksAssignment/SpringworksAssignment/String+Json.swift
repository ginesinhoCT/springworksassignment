//
//  File.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation

extension String {
    
    /**
     Serialize a json string in to an optional Dictionary.
     
     - returns:
     A *Dictionary<String, Any>* with the serialized json if there are not errors or *nil* if there are.
     */
    func serializeJsonToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
