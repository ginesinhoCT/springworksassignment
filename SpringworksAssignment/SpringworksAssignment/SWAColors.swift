//
//  SWAColors.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation
import UIKit

struct SWAColors {

    static let lightBlue = UIColor(red: 81/255, green: 164/255, blue: 255/255, alpha: 1)
}
