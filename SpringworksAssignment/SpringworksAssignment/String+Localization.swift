//
//  String+Localization.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 16/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation

extension String {
    
    /// Easy way to localized a String, e.g., "MyText".localized
    var localized: String! {
        let localizedString = NSLocalizedString(self, comment: "")
        return localizedString
    }
    
    /**
     Concatenate a localizated string with a string with info associated to ti.
     
     - parameters:
     - info: String of the info to show with the localizated string
     
     - returns:
     A formatted *String* with a localizated string and its info.
     
     - important:
     The compiler doesn't find it when we call it in a computer property's closure.
     */
    func localizedWith(info: String) -> String{
        return "\(self.localized!): \(info)"
    }
}
