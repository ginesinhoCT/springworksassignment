//
//  ViewController.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 14/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, APIManaging, AlertManaging, SWAReachability {
    
    // MARK: - IBOutlets
    
    //Title Labels
    @IBOutlet weak var mainInfoTitleLabel: UILabel!
    @IBOutlet weak var fuelTypesTitleLabel: UILabel!
    @IBOutlet weak var mainTitleTopBarLabel: UILabel!
    
    //Vehicle Info Labels
    @IBOutlet weak var brandLogoImage: UIImageView!
    @IBOutlet weak var vinLabel: UILabel!
    @IBOutlet weak var regnoLabel: UILabel!
    @IBOutlet weak var gearTypeLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    //Buttons
    @IBOutlet weak var reloadInfoButton: TopBarReloadButton!
    
    //Table View
    @IBOutlet weak var tableView: UITableView!
    
    //Other properties
    var fuelInfosArray: [Dictionary<String, Any>]? = nil
    var vehicleInfo: VehicleUSAInfoStruct? = nil
    
    
    // MARK: - Life clicle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        downloadVehicleInfo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - IBActions
    
    @IBAction func reloadInfoButtonTapped(_ sender: Any) {
        downloadVehicleInfo()
    }
    
    
    
    // MARK: - Private methods
    
    // MARK: - Set up
    
    private func setUp() {

        setUpTitleLabels()
        setUpDefaultInfo()
        setUpDefaultBranLogoImage()
        setUpTableView(self.fuelInfosArray)
    }
    
    private func setUpTitleLabels() {
        
        self.mainTitleTopBarLabel.text  = "MVTopBarTitle".localized
        self.mainInfoTitleLabel.text    = "MVMainInformationTitle".localized
        self.fuelTypesTitleLabel.text   = "MVFuelTypes".localized
    }
    
    private func setUpDefaultInfo() {
        
        self.vinLabel.text =  "MVVin".localizedWith(info: "n/a".localized)
        self.regnoLabel.text = "MVRegno".localizedWith(info: "n/a".localized)
        self.gearTypeLabel.text = "MVGearType".localizedWith(info: "n/a".localized)
        self.yearLabel.text = "MVYear".localizedWith(info: "n/a".localized)
    }
    
    private func setUpDefaultBranLogoImage() {
    
        self.brandLogoImage.image = UIImage(named: "defaultLogo")
    }
    
    private func setUpTableView(_ infoArray: [Any]?) {
        
        //We only enable scroll when there are more than one fuel type
        
        if let info = infoArray {
            if (info.count > 2) {
                self.tableView.isScrollEnabled = true
                return
            }
        }
        
        //We are loading the default info or only one fuel type
        self.tableView.isScrollEnabled = false
    }
    
    private func setUpVehicleMainInformation(_ vehicleInfo: VehicleUSAInfoStruct) {
        
        self.vinLabel.text =  "MVVin".localizedWith(info: vehicleInfo.vinString)
        self.regnoLabel.text = "MVRegno".localizedWith(info: vehicleInfo.regnoString)
        self.gearTypeLabel.text = "MVGearType".localizedWith(info: vehicleInfo.gearTypeString)
        self.yearLabel.text = "MVYear".localizedWith(info: vehicleInfo.yearString)
    }
    
    private func setUpVehicleBranchLogo(_ vehicleInfo: VehicleUSAInfoStruct) {
        
        self.brandLogoImage.image = vehicleInfo.brandLogoImage
    }

    private func reloadViewWith(_ vehicleInfo: VehicleUSAInfoStruct) {
        
        //Reload Main Vehicle Info
        self.setUpVehicleMainInformation(vehicleInfo)
        self.setUpVehicleBranchLogo(vehicleInfo)
        
        //Reload Table View
        self.fuelInfosArray = vehicleInfo.fuelInfosArray
        self.setUpTableView(self.fuelInfosArray)
        self.tableView.reloadData()
        
        //Save new timestamp
        self.setInUserDefaults(date: vehicleInfo.timestampDate, forkey: "savedVehicleInfoTimestamp")
    }
    
    
    // MARK: - Networking
    
    private func downloadVehicleInfo() {
        
        getRequest(SWAUrls.vehicleInfo, onCompletion: { [unowned self] infoDictionary, errorDescription in
        
            if let info = infoDictionary {
                
                self.vehicleInfo = VehicleUSAInfoStruct(dictionary: info)
                
                if !self.isConnectedToNetwork() {
                    
                    //We have cached data without internet connection. We reload the view with this data.
                    self.reloadViewWith(self.vehicleInfo!)
                    
                }else {
                    
                    //Check if we have to reload Vehicle Info using server timestamp
                    
                    guard let savedTimeStampDate = self.getDateFromUserDefaults(forkey: "savedVehicleInfoTimestamp") else {
                        //There is not local timestamp.
                        //Update view with vehicle info
                        self.reloadViewWith(self.vehicleInfo!)
                        return
                    }
                    
                    if (self.vehicleInfo?.timestampDate)! > savedTimeStampDate  {
                        //Server timestamp is newer
                        //Update view with vehicle info
                        self.reloadViewWith(self.vehicleInfo!)
                    }
                    
                    //We don't update view
                    return
                }
            } else if let description = errorDescription {
                self.showErrorAlert(error: "ErrorLoadingInfo".localized,message: description, delegate: self)
            }
        })
    }
}

