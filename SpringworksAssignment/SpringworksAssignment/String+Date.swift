//
//  String+Date.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import Foundation

extension String {
    
    /**
     Create a date in format iso-8601 from a string
     
     - returns:
     A *Date* in format iso-8601
     */
    func dateWithFormatISO8601() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter.date(from: self)!
    }
}
