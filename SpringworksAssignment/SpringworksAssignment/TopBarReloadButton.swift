//
//  TopBarReloadButton.swift
//  SpringworksAssignment
//
//  Created by Gines Sanchez Merono on 15/2/17.
//  Copyright © 2017 Ginés Sanchez. All rights reserved.
//

import UIKit

@IBDesignable

class TopBarReloadButton: UIButton {
    
    @IBInspectable var stroke: CGFloat = 3.0
    
    override func draw(_ rect: CGRect) {        
        SWAStyleKit.drawReloadBarButton(frame: rect, strokeColor: UIColor.white, strokeArrow: stroke)
    }
}
