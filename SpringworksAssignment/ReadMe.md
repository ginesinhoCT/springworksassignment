
SpringworksAssigment
============
SpringworksAssignment is an assignment for a company called Springworks.

Supporting Devices
=================== 

This app only supports iPhone devices.

Git Repository
===============

<https://ginesinhoCT@bitbucket.org/ginesinhoCT/springworksassignment.git>


External Framework Management
==============================

We are using for this purpose Cocoapod. The external frameworks we are using on this project are:

        - AlamoFire: Alamofire is an HTTP networking library written in Swift.        
        - Crashlytics: The most powerful, yet lightest weight crash reporting solution.
        - Fabric: Crashlytics manager.        

IMPORTANT: We don't add pods to the repository. You must run 'pod install' to install the pod files.


Networking
=============

We use Alamofire in our default implementation. We don't check Internet reachabilitybecause we manage the request errors and we can know when an error occurs because there is not Internet connection. We can add this functionality using Alamofire 'NetworkReachabilityManager'.

Localization
===========

This project is localizated to two languajes: English and Spanish. The Localizable.string files are in Resources/Strings.

Model
======

The model is formed by the VehicleInfo protocol (definition) and the VehicleInfoStruct (implementation). Also there are computed properties that provide data that to the view controller.

This implementation is based on the MVVM design pattern where the ‘public interface’ (we can’t hide the private implementation with a struct) of the class is the View Model. It’s this data what we test in the Unit tests.

Views
=====

We use a storyboard with one view controller for the Main View of the app. The storyboard is in the ‘Storyboards’ group/folder. The view controller that manages this view is ‘MainViewController’ and it and its extensions are in the ‘Controllers’ group/folder. 

The Main View is formed by three main elements:

    -	Brand logo image (content view and image view).
    -	Main information view.
    -	Table View for the fuel consumptions and emissions.

This view has customized size classes for:

    - wC hC
    - wC hR
    - wR hC

Extensions
===========

We create the next class extenions for making easier compute data:

- String+Json: serialize json strings.
- String+Date: convert date in string in date objects.
- String+Localization: localize strings.
- Number+MetricSystem: manage number conversion between different metric systems.
- Float+FormattedString: Format float in the right formatted string.

Using the Sample
================

IMPORTANT: We don't add pods to the repository. You must run 'pod install' to install the pod files before run the app.

Build and run the sample using Xcode:

A - If there is internet connection:

    1. The vehicle info is loaded.
    2. Click on reload vehicle info button.

    - If there is internet connection:  
        3. The vehicle info is reloaded if the new timestamp is newest that the stored one.

    - If there is not internet connection:
        - if there is cached data:
            3. Loads cached data.
        - if there is not cached data:
            3. An alert view shows the error if there is not cached data.

B - If there is not internet connection:
    - if there is cached data:
        1. Loads cached data.
        2. Click on reload vehicle info button.
            - If There is internet connection:
                3. The vehicle info is reloaded if the new timestamp is newest that the stored one.
            - If There is not internet connection:
                3. Loads cached data.

    - if there is not cached data:
        1. An error alert view is showed.
    
        - If reload vehicle info button is pressed:
            - If There is internet connection:
                2. The vehicle info is loaded.
            - If There is not internet connection:
                2. An error alert view is showed.

Main Files
==========

MainViewController.swift
-Main view controller that displays the vehicle info.

APIManaging.swift
- Protocol that provides the implementation for api networking.

SWAReachability.swift
- Protocol that provides the implementation for checking internet connection.

AlertManaging.swift
- Protocol that provides the implementation for showing alert views.

VehicleInfo.swift
- Protocol and struct that provide the model definition and implementation.

=============================================
Copyright (C) Ginés Sánchez. All rights reserved.

